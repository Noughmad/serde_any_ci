FROM rustlang/rust:nightly

RUN apt-get update && apt-get install -y --no-install-recommends \
    build-essential \
    cmake \
    pkg-config \
    libssl-dev \
    linkchecker \
 && rm -rf /var/lib/apt/lists/*

RUN cargo install cargo-tarpaulin
